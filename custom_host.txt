firebase-settings.crashlytics.col
www.expressapiv2.com

# korea
lbs.knbank.co.kr
onestore.co.kr
xo.fxshield.co.kr

# nordvpn
icpsuawn1zy5amys.com
x9fnzrtl4x8pynsf.com

# sharewareonsale
email.sharewareonsale.com
rs-stripe.sharewareonsale.com

# traveloka
feabtest.prod.tvlk-data.com
tracking.prod.tvlk-data.com

# zing, zalo
failover.zingmp3.vn
friend.talk.zing.vn
qos.talk.zing.vn
