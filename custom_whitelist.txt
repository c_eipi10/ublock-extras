! Title: Custom whitelist

@@||abs.twimg.com/responsive-web/client-web/vendors~main.63ba3ef5.js$script,domain=twitter.com
@@||apis.google.com/js/googleapis.proxy.js$script,domain=clients6.google.com
@@||archive.org/components/npm/@webcomponents/webcomponentsjs/webcomponents-bundle.js$script,strict1p
@@||archive.org/includes/build/npm/jquery/dist/jquery.min.js$script,strict1p
@@||archive.org/includes/build/npm/jquery-ui-dist/jquery-ui.min.js$script,strict1p
@@||cdn.datatables.net/*/js/jquery.dataTables.min.js$script,domain=techgaun.github.io
! @@||cdn.embedly.com/js/all.3e3f0dc1.js$script
@@||cdnjs.cloudflare.com/ajax/libs/axios/*/axios.min.js$script,domain=toptal.com
@@||cdnjs.cloudflare.com/ajax/libs/moment.js/*/moment.min.js$script,domain=techgaun.github.io
@@||cdnjs.cloudflare.com/ajax/libs/vue/*/vue.min.js$script,domain=toptal.com
@@||code.jquery.com/jquery-*.slim.min.js$script,domain=techgaun.github.io
@@||code.jquery.com/jquery-3.3.1.min.js$script,domain=jsonkeeper.com
@@||code.jquery.com/jquery.min.js$script,domain=vn2-p-dns.abpvn.com
@@||connect.facebook.net/en_US/sdk.js$script,domain=voz.vn
! @@||embed.redditmedia.com/widgets/platform.js$script
@@||github.githubassets.com/assets/chunk-cookies-48f59dde.js$script,domain=github.com
@@||p26.zdassets.com/hc/theming_assets/841909/617778/script.js$script,domain=support.signal.org
@@||platform.instagram.com/en_US/embeds.js$script,domain=voz.vn
@@||platform.twitter.com/embed/embed.modules.66e311263622456867b1.js$script,strict1p
@@||platform.twitter.com/embed/embed.vendors~ondemand.TimelineList~ondemand.TimelineProfile~ondemand.Tweet.29715ca2e27538cc4c88.js$script,strict1p
@@||platform.twitter.com/widgets.js$script,domain=voz.vn
@@||simgbb.com/3515/jquery2.js$script,domain=imgbb.com
@@||slice.vanilla.futurecdn.net/1-1-1//react.js$script,domain=laptopmag.com|tomsguide.com
@@||static.zdassets.com/hc/assets/jquery-d5395f0b7ac5027403fc17855c46dbfc.js$script,domain=support.signal.org
@@||static.zdassets.com/hc/assets/moment-f6f8513da6ab17eadada59a1a4edb536.js$script,domain=support.signal.org
@@||translate.google.com/translate_a/element.js$script,domain=translate.googleusercontent.com
@@||unpkg.com/react-dom@*/umd/react-dom.production.min.js$script,domain=patreon.com
@@||www.instagram.com/embed.js$script,domain=voz.vn
@@||www.instagram.com/static/bundles/es6/Vendor.js/48e0f28aa478.js$script,strict1p
